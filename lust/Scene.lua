require 'lust.utilities.Class'

local List = require 'lust.utilities.List'
local TextureCache = require 'lust.graphics.TextureCache'
local Mouse = require 'lust.input.Mouse'
local Keyboard = require 'lust.input.Keyboard'
local Rectangle = require 'lust.geometry.Rectangle'

local Scene = Class(function(self)
	self.process = nil
	self.entities = List()
	self.ecache = {}
	self.mouse = Mouse()
	self.keyboard = Keyboard()
	self.textures = TextureCache()
	self.camera = Rectangle()
end)

function Scene:focusIn()
end

function Scene:focusOut()
end

function Scene:update(dt)
	for i = 1, self.entities:size(), 1 do
		entity = self.entities:get(i)
		entity:update(dt)
	end
end

function Scene:render()
	for i = 1, self.entities:size(), 1 do
	--for i = self.entities:size(), 1, -1 do
		entity = self.entities:get(i)
		entity:render()
	end
end

function Scene:add(entity)
	if self.entities:indexOf(entity) > 0 then
		return
	elseif entity == nil then
		error("(Scene:add): Entity to be added cannot be nil.");
	end

	self.entities:push(entity)

	if entity.type ~= nil then
		if self.ecache[entity.type] == nil then
			self.ecache[entity.type] = List()
		end
		self.ecache[entity.type]:push(entity)
		entity.scene = self
		entity:added()
	end
end

function Scene:remove(entity)
	if entity == nil then 
		error("(Scene:remove): Entity to be removed cannot be nil.")
		return 
	end

	index = self.entities:indexOf(entity)
	if index > 0 then
		self.entities:slice(index)
		self.ecache[entity.type]:slice(self.ecache[entity.type]:indexOf(entity))
		if self.ecache[entity.type]:size() == 0 then
			self.ecache[entity.type] = nil
		end
		entity:removed()
		entity.scene = nil
	end
end

function Scene:getEntities(type)
	if type == nil then return self.entities end
	if self.ecache[type] ~= nil then
		return self.ecache[type]
	end
	return nil
end

function Scene:onCamera(x, y)
	if self.camera:xyInside(x, y) then
		return true
	end
	return false
end

return Scene