require 'lust.utilities.Class'

local Contact = require 'lust.input.Contact'

local Mouse = Class(function(self)
	self.left = Contact()
	self.right = Contact()
	self.middle = Contact()
end)

return Mouse