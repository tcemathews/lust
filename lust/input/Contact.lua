require 'lust.utilities.Class'

local Contact = Class(function(self)
	self.delta = {x=0.0, y=0.0, z=0.0}
	self.position = {x=0.0, y=0.0, z=0.0}
	self.active = 0
	self.activated = false
	self.deactivated = false
	self.preventsNext = false
end)

function Contact:update(x, y, z, active)
	self.preventsNext = false
	self.delta.x = x - self.position.x
	self.delta.y = y - self.position.y
	self.delta.z = z - self.position.z
	self.position.x = x
	self.position.y = y
	self.position.z = z
	self.activated = false
	self.deactivated = false

	if self.active and active ~= true then
		self.deactivated = true
	elseif self.active ~= true and active then
		self.activated = true
	end

	self.active = active
end

function Contact:preventNext()
	self.preventsNext = true
end

return Contact