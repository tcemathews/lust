require 'lust.utilities.Class'

local Keyboard = Class(function(self)
	self.pcache = {}
	self.rcache = {}
	self.hcache = {}
end)

Keyboard.UP = "up"
Keyboard.DOWN = "down"
Keyboard.RIGHT = "right"
Keyboard.LEFT = "left"

function Keyboard:update(dt)
	self.pcache = {}
	self.rcache = {}
end

function Keyboard:press(key, unicode)
	self.pcache[key] = true
	self.hcache[key] = true
end

function Keyboard:release(key, unicode)
	self.rcache[key] = true
	self.hcache[key] = false
end

function Keyboard:pressed(key)
	return self.pcache[key] == true
end

function Keyboard:released(key)
	return self.rcache[key] == true
end

function Keyboard:held(key)
	return self.hcache[key] == true
end

return Keyboard