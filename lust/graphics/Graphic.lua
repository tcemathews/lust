require 'lust.utilities.Class'

local Rectangle = require 'lust.geometry.Rectangle'

local Graphic = Class(function(self)
	self.x = 0.0
	self.y = 0.0
	self.width = 0.0
	self.height = 0.0
	self.rotation = 0.0
	self.origin = {x=0.0, y=0.0}
	self.scale = {x=1.0, y=1.0}
	self.shear = {x=0.0, y=0.0}
	self.color = {255, 255, 255, 255}
	self.visible = true
end)

function Graphic:drawDebugBox(ox, oy) 
	love.graphics.setColor({0, 255, 0, 0.9 * 255})
	love.graphics.rectangle("fill", ox + self.x, oy + self.y, self.width, self.height)
end

function Graphic:drawDebugBoundry(ox, oy) 
	love.graphics.setColor({0, 255, 0, 0.9 * 255})
	love.graphics.rectangle("line", ox + self.x, oy + self.y, self.width, self.height)
end

function Graphic:setColor(r, g, b, a)
	self.color[1] = r * 255
	self.color[2] = g * 255
	self.color[3] = b * 255
	self.color[4] = a * 255
end

function Graphic:setAlpha(value)
	self.color[4] = value * 255
end

function Graphic:generateHitbox()
	return Rectangle(self.x - (self.origin.x * self.scale.x), self.y - (self.origin.y * self.scale.y), self.width, self.height)
end

function Graphic:draw(ox, oy)
	love.graphics.setColor(self.color)
end

return Graphic