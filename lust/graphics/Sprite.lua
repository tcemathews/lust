local Graphic = require "lust.graphics.Graphic"

local Sprite = Class(Graphic, function(self)
	Graphic.init(self)
	self.anchor = {x=0.0, y=0.0}
	self.frames = {}
	self.animations = {}
	self.currentFrame = nil
	self.currentFrameIndex = 0
	self.currentAnimation = nil
	self.playing = false
	self.lapse = 0.0
end)

function Sprite:addFrame(key, texture, x, y, width, height)
	self.frames[key] = {texture=texture, clip=love.graphics.newQuad(x, y, width, height, texture.width, texture.height)}
end

function Sprite:addAnimation(key, frames, dt)
	self.animations[key] = {frames=frames, speed=dt}
end

function Sprite:play(key)
	if self.animations[key] == nil then return end
	self.currentAnimation = key
	self.currentFrameIndex = 1
	self.currentFrame = self.animations[self.currentAnimation].frames[self.currentFrameIndex]
	self:calculateAttr()
	self.playing = true
end

function Sprite:stop()
	self.playing = false
	self.currentAnimation = nil
	self.currentFrameIndex = 0
end

function Sprite:setFrame(key)
	if self.frames[key] ~= nil then
		self:stop()
		self.currentFrameIndex = 0
		self.currentFrame = key
		self:calculateAttr()
	end
end

function Sprite:update(dt)
	if self.currentAnimation ~= nil then
		local animation = self.animations[self.currentAnimation]
		self.lapse = self.lapse + dt
		while self.lapse > animation.speed do
			self.lapse = self.lapse - animation.speed
			self.currentFrameIndex = self.currentFrameIndex + 1
			if self.currentFrameIndex > #animation.frames then
				self.currentFrameIndex = 1
			end
			self.currentFrame = animation.frames[self.currentFrameIndex]
			self:calculateAttr()
		end
	end
end

function Sprite:calculateAttr()
	if self.currentFrame == nil then return end
	quad = self.frames[self.currentFrame].clip
	x, y, w, h = quad:getViewport()
	self.width = w * self.scale.x
	self.height = h * self.scale.y

	if self.anchor == nil then return end
	self.origin.x = w * self.anchor.x
	self.origin.y = h * self.anchor.y
end

function Sprite:draw(ox, oy)
	Graphic.draw(self, ox, oy)
	if self.currentFrame ~= nil then
		texture = self.frames[self.currentFrame].texture
		quad = self.frames[self.currentFrame].clip
		love.graphics.drawq(texture.image, quad, ox + self.x, oy + self.y, self.rotation, self.scale.x, self.scale.y, self.origin.x, self.origin.y, self.shear.x, self.shear.y)
	end
end

return Sprite