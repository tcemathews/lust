local Graphic = require "lust.graphics.Graphic"

local Picture = Class(Graphic, function(self, texture)
	Graphic.init(self)
	self.texture = nil
	self.frame = nil

	if texture then
		self:setTexture(texture)
	end
end)

function Picture:setTexture(texture)
	self.texture = texture

	if self.texture == nil then
		self.width = 0.0
		self.height = 0.0
		self.quad = nil
	else
		self.width = self.texture.width
		self.height = self.texture.height
		self.quad = love.graphics.newQuad(0, 0, self.width, self.height, self.width, self.height)
	end
end

function Picture:draw(ox, oy)
	Graphic.draw(self, ox, oy)
	if self.texture then
		love.graphics.drawq(self.texture.image, self.quad, ox + self.x, oy + self.y, self.rotation, self.scale.x, self.scale.y, self.origin.x, self.origin.y, self.shear.x, self.shear.y)
	end
end

return Picture