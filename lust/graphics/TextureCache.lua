Cache = require "lust.utilities.Cache"
Texture = require "lust.graphics.Texture"

local TextureCache = Class(Cache, function(self)
	Cache.init(self)
end)

function TextureCache:aget(filename)
	if self.cache[filename] then 
		texture = self.cache[filename]
	else
		texture = Texture(filename)
		self:set(filename, texture)
	end

	return texture
end

return TextureCache