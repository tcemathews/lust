require 'lust.utilities.Class'

local Rectangle = Class(function(self, x, y, width, height)
	self.x = x or 0
	self.y = y or 0
	self.width = width or 0
	self.height = height or 0
end)

function Rectangle:clone()
	rect = Rectangle(self.x, self.y, self.width, self.height)
	return rect
end

function Rectangle:copy(rect)
	self.x = rect.x
	self.y = rect.y
	self.width = rect.width
	self.height = rect.height
end

function Rectangle:pointInside(p)
	return self:xyInside(p.x, p.y)
end

function Rectangle:xyInside(x, y)
	if x >= self.x and x <= self.x + self.width and
		y >= self.y and y <= self.y + self.height then
		return true
	end

	return false
end

return Rectangle