local Entity = require "lust.Entity"
local Rectangle = require "lust.geometry.Rectangle"

local View = Class(Entity, function(self, picture)
	Entity.init(self)
	self.type = "View"
	self.frame = Rectangle(0, 0, 0, 0)
end)

return View