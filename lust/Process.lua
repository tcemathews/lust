require 'lust.utilities.Class'

local Process = Class(function(self)
	self.scene = nil -- Current scene we will update and render.
	self.wheelDelta = 0 -- Mouse wheel delta tracker.
end)

function Process:show(scene)
	if self.scene ~= nil then
		self.scene:focusOut()
		self.process = nil
		self.scene = nil
	end
	self.scene = scene
	self.scene.process = self
	self.scene.camera.width = love.graphics.getWidth()
	self.scene.camera.height = love.graphics.getHeight()
	self.scene:focusIn()
end

function Process:update(dt)
	if self.scene ~= nil then
		if self.scene.mouse ~= nil then
			local mx, my = love.mouse.getPosition()
			self.scene.mouse.left:update(mx, my, 0, love.mouse.isDown("l"))
			self.scene.mouse.right:update(mx, my, 0, love.mouse.isDown("r"))
			self.scene.mouse.middle:update(mx, my, self.wheelDelta, love.mouse.isDown("m"))
		end
		self.scene:update(dt)
		if self.scene.keyboard ~= nil then
			self.scene.keyboard:update(dt)
		end
	end
end

function Process:render()
	if self.scene ~= nil then
		self.scene:render()
	end
end

function Process:mousePressed(x, y, mouseButton)
	if mouseButton == "wu" then
		self.wheelDelta = self.wheelDelta + 1
	elseif mouseButton == "wd" then
		self.wheelDelta = self.wheelDelta - 1
	end
end

function Process:keyPressed(key, unicode)
	if self.scene ~= nil and self.scene.keyboard ~= nil then
		self.scene.keyboard:press(key, unicode)
	end
end

function Process:keyReleased(key, unicode)
	if self.scene ~= nil and self.scene.keyboard ~= nil then
		self.scene.keyboard:release(key, unicode)
	end
end

return Process